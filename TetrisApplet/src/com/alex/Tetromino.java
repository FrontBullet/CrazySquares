package com.alex;


public class Tetromino implements Constants{

	
	private Tetrominoes pieceShape;
	private int x;
	private int y;
	private boolean[][] piecePixls = new boolean[4][4];
	private boolean[][] tmpPixls  = new boolean[4][4];
	private int pieceColor;

	public Tetromino(Tetrominoes shape){
		
		pieceShape = shape;
		piecePixls = T_GRID[shape.ordinal()];
		pieceColor = shape.ordinal()+1;
	}


	public Tetrominoes getPieceShape() {
		return pieceShape;
	}

	public void setPieceShape(Tetrominoes pieceShape) {
		this.pieceShape = pieceShape;
	}
	
	public int getPieceColor() {
		return pieceColor;
	}

	public void setPieceColor(int pieceColor) {
		this.pieceColor = pieceColor;
	}
	
	public boolean[][] getPiecePixls() {
		return piecePixls;
	}

	public void setPiecePixls(boolean[][] piecePixls) {
		this.piecePixls = piecePixls;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void rotateLeft() {
		tmpPixls = new boolean[4][4];
		for(int i=0; i<4; i++)
			for(int j=0; j<4; j++)
				tmpPixls[i][j] = piecePixls[j][3-i];
		
		piecePixls = tmpPixls.clone();
	}
	
	public void rotateRight() {		
		tmpPixls = new boolean[4][4];
		for(int i=0; i<4; i++)
			for(int j=0; j<4; j++)
				tmpPixls[i][j] = piecePixls[3-j][i];
		
		piecePixls = tmpPixls.clone();
	}
	
}

