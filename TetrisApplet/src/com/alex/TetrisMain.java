package com.alex;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import javax.swing.Timer;


@SuppressWarnings("serial")
public class TetrisMain extends JPanel implements Constants, ActionListener, FocusListener{

	TetrisBoard gameBoard;
	Timer gameClock;
	List<Integer> clearLines;
	
	SpringLayout gameLayout;
	BoxLayout menuLayout;
	
	int score = 0;
	int gameStatus = 0;
	
	JLabel scoreDisplay;
	JLabel scoreLabel;
	JLabel mainTitle;
	JButton startNormalButton;
	JButton startHardButton;
	JButton startImpossibleButton;
	JButton startEasyButton;
	JButton startWeirdButton;
	JButton startWeirderButton;
	JButton closeButton;
	
	public static void main(String[] args) throws InterruptedException{
		
		JFrame frame = new JFrame("Tetris");
        TetrisMain game = new TetrisMain();
        frame.add(game);
        frame.setLocation(500, 10);
        frame.setSize(600, 800);
        frame.setVisible(true);
        frame.setBackground(Color.CYAN);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        game.init();
        
	}
	
	private void init(){
				
		initMenuUI();
		
	}
	
	public void initMenuUI(){
		
		this.removeAll();
		
		this.setBackground(new Color(0xD0FFE0));
		
		menuLayout = new BoxLayout(this,BoxLayout.Y_AXIS);
		this.setLayout(menuLayout);
		
		mainTitle = new JLabel("Crazy Squares");
		mainTitle.setFont(new Font("Arial",Font.BOLD,50));
		this.add(mainTitle);
		
		this.add(Box.createRigidArea(new Dimension(20,80)));
		
		startEasyButton = new JButton("Easy");
		startEasyButton.setFont(new Font("Arial",0,30));
		startEasyButton.setForeground(Color.BLUE);
		startEasyButton.setMargin(new Insets(0, 0, 0, 0));
		startEasyButton.setContentAreaFilled(false);
		startEasyButton.setBorderPainted(false);
        setOpaque(false);
		this.add(startEasyButton);
		startEasyButton.addActionListener(this);
		startEasyButton.addFocusListener(this);
		

		this.add(Box.createRigidArea(new Dimension(20,10)));
		
		startNormalButton = new JButton("Normal");
		startNormalButton.setFont(new Font("Arial",0,30));
		startNormalButton.setForeground(Color.BLUE);
		startNormalButton.setMargin(new Insets(0, 0, 0, 0));
		startNormalButton.setContentAreaFilled(false);
		startNormalButton.setBorderPainted(false);
        setOpaque(false);
		this.add(startNormalButton);
		startNormalButton.addActionListener(this);
		startNormalButton.addFocusListener(this);
		
		this.add(Box.createRigidArea(new Dimension(20,10)));

		startHardButton = new JButton("Hard");
		startHardButton.setFont(new Font("Arial",0,30));
		startHardButton.setForeground(Color.BLUE);
		startHardButton.setMargin(new Insets(0, 0, 0, 0));
		startHardButton.setContentAreaFilled(false);
		startHardButton.setBorderPainted(false);
        setOpaque(false);
		this.add(startHardButton);
		startHardButton.addActionListener(this);
		startHardButton.addFocusListener(this);
		
		this.add(Box.createRigidArea(new Dimension(20,10)));
		
		startImpossibleButton = new JButton("Impossible");
		startImpossibleButton.setFont(new Font("Arial",0,30));
		startImpossibleButton.setForeground(Color.BLUE);
		startImpossibleButton.setMargin(new Insets(0, 0, 0, 0));
		startImpossibleButton.setContentAreaFilled(false);
		startImpossibleButton.setBorderPainted(false);
        setOpaque(false);
		this.add(startImpossibleButton);
		startImpossibleButton.addActionListener(this);
		startImpossibleButton.addFocusListener(this);
		
		this.add(Box.createRigidArea(new Dimension(20,10)));
		
		startWeirdButton = new JButton("Weird");
		startWeirdButton.setFont(new Font("Arial",0,30));
		startWeirdButton.setForeground(Color.BLUE);
		startWeirdButton.setMargin(new Insets(0, 0, 0, 0));
		startWeirdButton.setContentAreaFilled(false);
		startWeirdButton.setBorderPainted(false);
        setOpaque(false);
		this.add(startWeirdButton);
		startWeirdButton.addActionListener(this);
		startWeirdButton.addFocusListener(this);
		
		this.add(Box.createRigidArea(new Dimension(20,10)));
		
		startWeirderButton = new JButton("Weirder");
		startWeirderButton.setFont(new Font("Arial",0,30));
		startWeirderButton.setForeground(Color.BLUE);
		startWeirderButton.setMargin(new Insets(0, 0, 0, 0));
		startWeirderButton.setContentAreaFilled(false);
		startWeirderButton.setBorderPainted(false);
        setOpaque(false);
		this.add(startWeirderButton);
		startWeirderButton.addActionListener(this);
		startWeirderButton.addFocusListener(this);
		
		this.add(Box.createVerticalGlue());
		
		closeButton = new JButton("Exit");
        closeButton.setFont(new Font("Arial",0,30));
        closeButton.setForeground(Color.RED);
        closeButton.setMargin(new Insets(0, 0, 0, 0));
		closeButton.setContentAreaFilled(false);
		closeButton.setBorderPainted(false);
        this.add(closeButton);
        closeButton.addActionListener(this);
        closeButton.addFocusListener(this);
		
		mainTitle.setAlignmentX(CENTER_ALIGNMENT);
		closeButton.setAlignmentY(BOTTOM_ALIGNMENT);
		closeButton.setAlignmentX(CENTER_ALIGNMENT);
		startNormalButton.setAlignmentX(CENTER_ALIGNMENT);
		startHardButton.setAlignmentX(CENTER_ALIGNMENT);
		startImpossibleButton.setAlignmentX(CENTER_ALIGNMENT);
		startEasyButton.setAlignmentX(CENTER_ALIGNMENT);
		startWeirdButton.setAlignmentX(CENTER_ALIGNMENT);
		startWeirderButton.setAlignmentX(CENTER_ALIGNMENT);
		
		
		this.validate();
        
        startNormalButton.requestFocusInWindow();
        
		Set<KeyStroke> set1 = new HashSet<KeyStroke>();
        set1.add(KeyStroke.getKeyStroke("UP"));
        this.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, set1);
        
        Set<KeyStroke> set2 = new HashSet<KeyStroke>();
        set2.add(KeyStroke.getKeyStroke("DOWN"));
        this.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, set2);
        
        this.getInputMap(IFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0), ENTER);
        this.getActionMap().put(ENTER, new MenuAction(ENTER));
        this.getInputMap(IFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0), ESC);
        this.getActionMap().put(ESC, new MenuAction(ESC));
	}
	
	public void initBoardUI(){
		
		this.removeAll();
		
		this.validate();
		gameLayout = new SpringLayout();
		
		this.setLayout(gameLayout);

		scoreDisplay = new JLabel();
        scoreDisplay.setFont(new Font("Arial",Font.BOLD | Font.ITALIC,30));
        scoreDisplay.setText("0");
        this.add(scoreDisplay);
        
        scoreLabel = new JLabel(Integer.toString(score));
        scoreLabel.setFont(new Font("Arial",Font.BOLD,30));
        scoreLabel.setText("Score:");
        this.add(scoreLabel);
        
        closeButton = new JButton("Exit");
        closeButton.setFont(new Font("Arial",0,30));
        closeButton.setMargin(new Insets(0, 0, 0, 0));
		closeButton.setContentAreaFilled(false);
		closeButton.setBorderPainted(false);
        this.add(closeButton);
        closeButton.addActionListener(this);
        
        gameLayout.putConstraint(SpringLayout.EAST, scoreDisplay, -10, SpringLayout.EAST, this);
        gameLayout.putConstraint(SpringLayout.NORTH, scoreDisplay, 10, SpringLayout.NORTH, this);
        
        gameLayout.putConstraint(SpringLayout.EAST, scoreLabel, -20, SpringLayout.WEST, scoreDisplay);
        gameLayout.putConstraint(SpringLayout.NORTH, scoreLabel, 0, SpringLayout.NORTH, scoreDisplay);
        
        gameLayout.putConstraint(SpringLayout.EAST, closeButton, -10, SpringLayout.EAST, this);
        gameLayout.putConstraint(SpringLayout.SOUTH, closeButton, -10, SpringLayout.SOUTH, this);
		
		this.getInputMap(IFW).put(KeyStroke.getKeyStroke("LEFT"), Direction.Left);
        this.getActionMap().put(Direction.Left, new MoveAction(Direction.Left));
        
        this.getInputMap(IFW).put(KeyStroke.getKeyStroke("DOWN"), Direction.Down);
        this.getActionMap().put(Direction.Down, new MoveAction(Direction.Down));
        
        this.getInputMap(IFW).put(KeyStroke.getKeyStroke("UP"), Direction.Up);
        this.getActionMap().put(Direction.Up, new PieceAction(ROTATE_RIGHT));
        
        this.getInputMap(IFW).put(KeyStroke.getKeyStroke("RIGHT"), Direction.Right);
        this.getActionMap().put(Direction.Right, new MoveAction(Direction.Right));
        
        this.getInputMap(IFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_Z,0), ROTATE_LEFT);
        this.getActionMap().put(ROTATE_LEFT, new PieceAction(ROTATE_LEFT));
        
        this.getInputMap(IFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_X,0), ROTATE_RIGHT);
        this.getActionMap().put(ROTATE_RIGHT, new PieceAction(ROTATE_RIGHT));
        
        this.getInputMap(IFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE,0), DROP_PIECE);
        this.getActionMap().put(DROP_PIECE, new PieceAction(DROP_PIECE));
        
        this.getInputMap(IFW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0), ESC);
        this.getActionMap().put(ESC, new MenuAction(ESC));
        
        this.validate();

	}
	
	public void initGame(int mode){
		
		initBoardUI();
		
		gameBoard = new TetrisBoard(20, 10, this, mode);
		if(mode == 2)
			gameClock = new Timer(500, this);
		else if(mode == 3)
			gameClock = new Timer(100, this);
		else
			gameClock = new Timer(1000, this);
		
		gameClock.setInitialDelay(100);
		
        gameStatus = mode;
        gameClock.start();

		score = 0;
	}
	
	private void gameStep(){
		
		if(gameStatus!=6)
			gameBoard.moveShape(Direction.Down);
		else
			gameBoard.moveShape(Direction.Right);
		if(gameBoard.isLanded()){
			clearLines = gameBoard.isLineCompleted();
			if(!clearLines.isEmpty()){
				score+=(clearLines.size()*100);
				scoreDisplay.setText(Integer.toString(score));
				gameClock.stop();
				flashLines(clearLines);
				gameBoard.clearLines(clearLines);
				gameClock.restart();
			}
			if(!gameBoard.newShape()){
				gameOver();
			}
		}
		
		repaint();
		
	}

	public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        if(gameStatus!=0)
        	gameBoard.paint(g2d);
        
    }

	public void flashLines(List<Integer> lines){
		try{
			gameBoard.switchLines(lines);
			this.paintImmediately(0, 0, 600, 800);
			Thread.sleep(200);
			gameBoard.switchLines(lines);
			this.paintImmediately(0, 0, 600, 800);
			Thread.sleep(200);
			gameBoard.switchLines(lines);
			this.paintImmediately(0, 0, 600, 800);
			Thread.sleep(200);
			gameBoard.switchLines(lines);
			this.paintImmediately(0, 0, 600, 800);
			Thread.sleep(200);
		}catch(Exception e){
			System.out.println("exception!" + e.getStackTrace());
		}
	}
	
	public void gameOver(){
		List<Integer> lines = new ArrayList<Integer>();
		for(int i=0 ; i < BOARD_HEIGHT ; i++){
			lines.add(i);
		}
		flashLines(lines);
		score=0;
		gameStatus=0;
		gameBoard.resetBoard();
		initMenuUI();
		
	}

	@Override
	/**
	 * Triggered by gameClock or menu buttons.
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(closeButton)){
			System.exit(0);
		}else if(e.getSource().equals(startEasyButton)){
			initGame(4);
		}else if(e.getSource().equals(startNormalButton)){
			initGame(1);
		}else if(e.getSource().equals(startHardButton)){
			initGame(2);
		}else if(e.getSource().equals(startImpossibleButton)){
			initGame(3);
		}else if(e.getSource().equals(startWeirdButton)){
			initGame(5);
		}else if(e.getSource().equals(startWeirderButton)){
			initGame(6);
		}else if(e.getSource().equals(gameClock)){
			gameStep();
			repaint();
		}
		
	}
	
	@Override
	public void focusGained(FocusEvent e) {
		JButton b = (JButton) e.getSource();
		b.setContentAreaFilled(true);
		b.setBackground(Color.DARK_GRAY);
		SwingUtilities.getRootPane(b).setDefaultButton(b);
	}

	@Override
	public void focusLost(FocusEvent e) {
		JButton b = (JButton) e.getSource();
		b.setContentAreaFilled(false);
		b.setBackground(Color.CYAN);
		
	}
	
	public void finish(){
		System.exit(0);
	}

	private class MoveAction extends AbstractAction {

		Direction direction;

        MoveAction(Direction d) {
        	direction = d;
        }

		@Override
		/**
		 * Triggered by keyStroke
		 */
		public void actionPerformed(ActionEvent e) {
			
			gameBoard.moveShape(direction);
			repaint();
			
		}
    }

	private class PieceAction extends AbstractAction {

		String action;

        PieceAction(String a) {
        	action = a;
        }

		@Override
		/**
		 * Triggered by keyStroke
		 */
		public void actionPerformed(ActionEvent e) {
			
			if(action.equals(ROTATE_LEFT)){
				gameBoard.rotateShape(ROTATE_LEFT);
			}else if(action.equals(ROTATE_RIGHT)){
				gameBoard.rotateShape(ROTATE_RIGHT);
			}else if(action.equals(DROP_PIECE)){
				gameBoard.dropPiece();
				gameStep();
			}
			repaint();
			
		}
    }
	
	private class MenuAction extends AbstractAction {

		String ma;

		MenuAction(String a) {
			ma = a;
		}

		@Override
		/**
		 * Triggered by keyStroke
		 */
		public void actionPerformed(ActionEvent e) {

			if(ma.equals(ESC)){
				score=0;
				gameStatus=0;
				gameBoard.resetBoard();
				initMenuUI();
			}else if(ma.equals(ENTER)){
				SwingUtilities.getRootPane(closeButton).getDefaultButton().doClick();
			}

		}
	}


	
}
