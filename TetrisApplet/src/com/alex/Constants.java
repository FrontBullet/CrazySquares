package com.alex;

import java.awt.Color;

import javax.swing.JComponent;

public interface Constants {

	static final int PIXEL_SIZE = 30;
	static final int BOARD_HEIGHT = 25;
	static final int BOARD_WIDTH = 10;
	
	
	static final int IFW = JComponent.WHEN_IN_FOCUSED_WINDOW;
    static final String MOVE_LEFT = "move left";
    static final String MOVE_DOWN = "move down";
    static final String MOVE_UP = "move up";
    static final String MOVE_RIGHT = "move right";
    static final String ROTATE_LEFT = "rotate left";
    static final String ROTATE_RIGHT = "rotate right";
    static final String DROP_PIECE = "drop piece";
    static final String ENTER = "enter";
    static final String ESC = "esc";
    
	
	enum Tetrominoes {Line, NL, L, S, Z, T, Square };
	enum Direction {Up, Down, Left, Right};
	
	public final static Color T_COLOR[] = {
		Color.black,
		new Color(0x00FFFF), 
		new Color(0x2E8B57),
		new Color(0xFFA500),
		new Color(0xFF0000),
		new Color(0xFFFF00),
		new Color(0xFF1493),
		new Color(0x6A5ACD)};
	
	
	
	public final static boolean T_GRID[][][] = {
		{
			{false, true, false, false},
			{false, true, false, false},
			{false, true, false, false},
			{false, true, false, false},
		},
		{
			{false, false, false, false},
			{false, true, true, false},
			{false, true, false, false},
			{false, true, false, false},
		},
		{
			{false, false, false, false},
			{false, true, false, false},
			{false, true, false, false},
			{false, true, true, false},
		},
		{
			{false, false, false, false},
			{false, true, false, false},
			{false, true, true, false},
			{false, false, true, false},
		},
		{
			{false, false, false, false},
			{false, false, true, false},
			{false, true, true, false},
			{false, true, false, false},
		},
		{
			{false, false, false, false},
			{false, true, false, false},
			{false, true, true, false},
			{false, true, false, false},
		},
		{
			{false, false, false, false},
			{false, true, true, false},
			{false, true, true, false},
			{false, false, false, false},
		},
	};
	
	public final static boolean T_WEIRD[][][] = {
		{
			{false, true, true, false},
			{false, true, true, false},
			{false, true, true, false},
			{false, true, true, false},
		},
		{
			{true, true, true, true},
			{false, false, true, false},
			{false, false, false, false},
			{false, false, false, false},
		},
		{
			{false, false, false, false},
			{false, true, false, false},
			{false, false, false, false},
			{false, false, false, false},
		},
		{
			{false, false, false, false},
			{false, true, false, false},
			{false, false, true, false},
			{false, false, false, false},
		},
		{
			{false, false, false, true},
			{false, false, true, true},
			{false, true, true, false},
			{true, true, false, false},
		},
		{
			{false, false, false, false},
			{false, true, false, false},
			{false, true, true, true},
			{false, true, false, false},
		},
		{
			{false, false, false, false},
			{false, true, true, false},
			{false, false, true, false},
			{false, false, false, false},
		},
	};
}
