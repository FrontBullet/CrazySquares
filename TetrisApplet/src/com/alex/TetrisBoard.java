package com.alex;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class TetrisBoard implements Constants{

	private final int pixSize = PIXEL_SIZE;
	private final int height = BOARD_HEIGHT;
	private final int width = BOARD_WIDTH;
	private int posx;
	private int posy;
	
	private Tetromino tmino;
	private boolean isLanded;
	private int gameMode;
	
	private int[][] Grid;
	
	public TetrisBoard(int x, int y, TetrisMain game, int mode){
				
		Grid = new int[height][width];
		posx = x;
		posy = y;
		isLanded = false;
		
		gameMode = mode;
		
		if(gameMode==4)	
			tmino = new Tetromino(Tetrominoes.Line);
		else
			tmino = new Tetromino(Tetrominoes.values()[(int) Math.floor(Math.random()*7)]);
		
		if(gameMode==5){
			tmino.setPiecePixls(T_WEIRD[tmino.getPieceShape().ordinal()]);
		}
		
		if(gameMode==6){
			tmino.setX(0);
			tmino.setY(height/2);
		}else{
			tmino.setX((width/2)-1);
			tmino.setY(0);
		}
		
	}
	
	public List<Integer> isLineCompleted(){
		boolean lineComplete = false;
		List<Integer> lines = new ArrayList<Integer>();
		for(int i=0 ; i < height ; i++){
			lineComplete = true;
			for(int j=0 ; j < width ; j++){
				if(Grid[i][j] == 0)
					lineComplete = false;
			}
			if(lineComplete){
				lines.add(i);
			}
		}
		return lines;
		
	}
	
	public void switchLines(List<Integer> lines){

		Iterator<Integer> iter = lines.iterator();
		while(iter.hasNext()){
			int i = (int) iter.next();
			for(int j=0 ; j < Grid[i].length ; j++){
				if(Grid[i][j] == 0)
					Grid[i][j] = 1;
				else
					Grid[i][j] = 0;
				
			}
		}
	}
	
	public void clearLines(List<Integer> lines){
		Iterator<Integer> iter = lines.iterator();
		while(iter.hasNext()){
			int i = (int) iter.next();
			for(int j=0 ; j < Grid[i].length ; j++){
				Grid[i][j] = 0;
			}
		}

		if(gameMode!=3){
			iter = lines.iterator();
			while(iter.hasNext()){
				int i = iter.next();
				for(int j = i; j>0 ; j--){
					Grid[j] = Grid[j-1].clone();
				}
			}
		}
	}
	
	public void resetBoard(){
		for(int i=0;i < width ;i++)
			for(int j=0; j < height ;j++)
				Grid[j][i] = 0;
	}
	
	
	public boolean newShape(){
		if(gameMode==4)	
			tmino = new Tetromino(Tetrominoes.Line);
		else
			tmino = new Tetromino(Tetrominoes.values()[(int) Math.floor(Math.random()*7)]);

		if(gameMode==5){
			tmino.setPiecePixls(T_WEIRD[tmino.getPieceShape().ordinal()]);
		}
		
		if(gameMode==6){
			tmino.setX(0);
			tmino.setY(height/2);
		}else{
			tmino.setX((width/2)-1);
			tmino.setY(0);
		}
		
		isLanded=false;
		
		for(int i=0;i < 4;i++)
			for(int j=0; j< 4;j++)
				if(tmino.getPiecePixls()[j][i])
					if(Grid[j+tmino.getY()][i+tmino.getX()] != 0)
						return false;
		
		return true;
	}
	
	public void moveShape(Direction d){
		cutShape();
		if(canMove(d)){
			switch(d){
			case Down:
				tmino.setY(tmino.getY() + 1);
				break;
			case Left:
				tmino.setX(tmino.getX() - 1);
				break;
			case Right:
				tmino.setX(tmino.getX() + 1);
				break;
			case Up:
				tmino.rotateRight();
				break;
			}
		}
		pasteShape();

	}
	
	public boolean canMove(Direction d){
		int x = tmino.getX();
		int y = tmino.getY();
		
		switch(d){
		case Down:
			for(int i=0;i < 4;i++)
				for(int j=0; j< 4;j++)
					if(tmino.getPiecePixls()[j][i])
						if(y+j+1>= height || Grid[y+j+1][x+i] != 0){
							isLanded = true;
							return false;
						}
			break;
			
		case Left:
			for(int i=0;i < 4;i++)
				for(int j=0; j< 4;j++)
					if(tmino.getPiecePixls()[j][i])
						if(x+i == 0 || Grid[y+j][x+i-1] != 0)
							return false;
			break;
		case Right:
			for(int i=0;i < 4;i++)
				for(int j=0; j< 4;j++)
					if(tmino.getPiecePixls()[j][i])
						if(x+i+1 >= width  || Grid[y+j][x+i+1] != 0){
							if(gameMode==6)
								isLanded=true;
							return false;
						}
			break;
		case Up:

			break;
		default:
			return true;
		}
		
		
		return true;
	}
	
	public void cutShape(){
		for(int i=0;i < 4;i++){
			for(int j=0; j< 4;j++){
				//System.out.print(tmino.getPieceColor());
				if(tmino.getPiecePixls()[j][i])
					Grid[tmino.getY() + j][tmino.getX() + i] = 0;
			}
			//System.out.println();
		}
	}
	
	public void pasteShape(){
		for(int i=0;i < 4;i++){
			for(int j=0; j< 4;j++){
				if(tmino.getPiecePixls()[j][i])
					Grid[tmino.getY() + j][tmino.getX() + i] = tmino.getPieceColor();
			}
			//System.out.println();
		}
	}
	
	public void rotateShape(String d){
		
		if(gameMode != 5 && tmino.getPieceShape().equals(Tetrominoes.Square))
			return;
		
		int x = tmino.getX();
		int y = tmino.getY();
		boolean offGrid = false;
		
		cutShape();

		if(d.equals(ROTATE_LEFT))
			tmino.rotateLeft();
		else if(d.equals(ROTATE_RIGHT))
			tmino.rotateRight();

		for(int i=0;i < 4;i++)
			for(int j=0; j< 4;j++)
				if(tmino.getPiecePixls()[j][i])
					if(y+j+1 >= height || x+i+1 >= width || x+i < 0 || Grid[y+j][x+i]!= 0)
						offGrid=true;

		if(offGrid){
			if(d.equals(ROTATE_LEFT))
				tmino.rotateRight();
			else if(d.equals(ROTATE_RIGHT))
				tmino.rotateLeft();
		}
		
		pasteShape();
	}
	
	public void dropPiece() {
		cutShape();
		while(true){
			if(canMove(Direction.Down))
				tmino.setY(tmino.getY() + 1);
			else
				break;
		}
		pasteShape();
	}

	
	public void paint (Graphics2D g){
		Color c = Color.black;
		
		for(int i=0;i < Grid.length;i++){
			for(int j=0; j< Grid[i].length;j++){
				
				c = T_COLOR[Grid[i][j]];
				
				g.setColor(c);
				g.fillRect(posx + (j*pixSize), posy + (i*pixSize), pixSize-2, pixSize-2);
				
				c = c.darker();
				c = c.darker();
				
				g.setColor(c);
				g.fillRect(posx + pixSize-2 + (j*pixSize), posy + (i*pixSize), 2, pixSize-1);
				g.fillRect(posx + (j*pixSize), posy + pixSize-2 + (i*pixSize), pixSize-1, 2);
				
			}
		}
	}


	public Tetromino getTmino() {
		return tmino;
	}


	public void setTmino(Tetromino tmino) {
		this.tmino = tmino;
	}


	public boolean isLanded() {
		return isLanded;
	}


	public void setLanded(boolean isLanded) {
		this.isLanded = isLanded;
	}


	
}
